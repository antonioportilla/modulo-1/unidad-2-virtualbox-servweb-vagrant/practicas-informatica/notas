<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Nota;

?>

<div class="col-sm-4 col-md-6">
        <div class="thumbnail">
        <h2><?= $model->fecha ?> </h2>
        <div class="bg-gris"></div>
        <div class="caption">
            <?php
            foreach ($model->getNotas() as $registro) {
               echo "<p>$registro->mensaje</p>";
               echo "<p>$registro->hora</p>";
            }
            ?>
        </div>
  </div>
